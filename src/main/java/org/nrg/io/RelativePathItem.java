/*
 * SessionBuilders: org.nrg.io.RelativePathItem
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.io;

public interface RelativePathItem {
  public String getRelativePath();
}
